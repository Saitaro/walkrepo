﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeFollow : MonoBehaviour
{
    public GameObject Boot;
    public GameObject RopeAnchor;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Boot.transform.position = RopeAnchor.transform.position;
    }
}
