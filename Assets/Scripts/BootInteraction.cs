﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BootInteraction : MonoBehaviour
{
    public LayerMask mask1;
    public LayerMask mask2;

    public float Sens;

    public GameObject ActiveBoot;
    public GameObject InactiveBoot;
    public GameObject LeftBoot;
    public GameObject RightBoot;
    float DeltaX;
    float DeltaZ;
    public float radius;

    // Start is called before the first frame update
    void Start()
    {
        ActiveBoot = RightBoot;
        InactiveBoot = LeftBoot;
    }

    // Update is called once per frame
    void Update()
    {


        if(Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            Vector3 touchPos = Camera.main.ScreenToWorldPoint(new Vector3 (touch.position.x, touch.position.y, Sens));

            switch (touch.phase)
            {

                case TouchPhase.Began:
                    DeltaX = touchPos.x - ActiveBoot.transform.position.x;
                    DeltaZ = touchPos.z - ActiveBoot.transform.position.z;
                    ActiveBoot.GetComponent<Animator>().SetBool("IsUp", true);
                    break;

                case TouchPhase.Moved:
                    ActiveBoot.transform.position = Vector3.ClampMagnitude(InactiveBoot.transform.position,radius);
                    ActiveBoot.GetComponent<Rigidbody>().MovePosition(new Vector3(touchPos.x - DeltaX, 1, touchPos.z - DeltaZ));
                    break;

                case TouchPhase.Ended:
                    ActiveBoot.GetComponent<Rigidbody>().velocity = Vector3.zero;
                    ActiveBoot.GetComponent<Animator>().SetBool("IsUp", false);
                    if (ActiveBoot == LeftBoot)
                    {
                        ActiveBoot = RightBoot;
                        InactiveBoot = LeftBoot;
                    }

                    else if (ActiveBoot == RightBoot)
                    {
                        ActiveBoot = LeftBoot;
                        InactiveBoot = RightBoot;
                    }

                    break;
            }
        }
    }


}
